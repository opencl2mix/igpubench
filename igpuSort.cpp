#include <stdio.h>
#include <cstring>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <iostream>
#include <string>
#include <chrono>
#include <map>
#include <algorithm>

#include "timsort.hpp"

#if __APPLE__
#else
#include <omp.h>
#endif

#include "helper.hpp"

using namespace std;

/************************************
	Print and check solution
************************************/
int printCheck(vector<float> vec)
{
    int numMistmatch = 0;
    int printStep = (vec.size() >> 3) + 1;

    if(GLOBAL.verbose) {
	    cout << endl << "Sample: ";
    } 
    
    for(unsigned int i = 1; i < vec.size(); i++) {
        if((i % printStep == 0) && (GLOBAL.verbose))
            cout << vec[i] << " ";
        if(vec[i - 1] > vec[i])
            numMistmatch++;
    }

    if(GLOBAL.verbose) {
        cout << endl << "Not sorted: " << numMistmatch << endl;
    }

    return numMistmatch;
}

/************************************
	GPU execution 
************************************/
class GPU
{
public:
	int ret;
	cl_kernel kernel;
    cl_kernel kernel_lds;
    cl_kernel kernel_bit2K;
    cl_kernel kernel_nolds;    

	GPU()
	{
		const char* kernel_cstr;
        string kernel_src;
		
		/* retrieve kernel source */
		read_kernel("sort.cl", kernel_src);
		kernel_cstr = kernel_src.c_str();	

		/* create kernel program from source */
		cl_program program = clCreateProgramWithSource(GLOBAL.clContext, 1,
			 &kernel_cstr, NULL, &ret);
		CL_ERR( ret );
    
		ret = clBuildProgram(program, 1, &GLOBAL.clDevice, NULL, NULL, NULL);
		CL_COMPILE_ERR( ret, program, GLOBAL.clDevice );

		/* create kernel associated to compiled source kernel*/
		kernel = clCreateKernel(program, "bitonic_sort", &ret);
		CL_ERR( ret );
        
        kernel_lds = clCreateKernel(program, "bitonic_sort_lds", &ret);
        CL_ERR( ret );
        
       	kernel_nolds = clCreateKernel(program, "bitonic_sort_nolds", &ret);
		CL_ERR( ret );
	
	}

	bool bitonicSort(float* ptrData, size_t elemNum)
	{
        float* ptrDataCheck = NULL;
        
        //GLOBAL.profiler.start("GPU-HOST-PTR");
        cl_mem gpu_mem = clCreateBuffer(GLOBAL.clContext,
                                        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(float) * elemNum, ptrData, &ret);
        //GLOBAL.profiler.stop("GPU-HOST-PTR");
        CL_ERR( ret );
        
        CL_ERR( clSetKernelArg(kernel, 0, sizeof(gpu_mem), &gpu_mem) );

		/* Each group of work-items processes a chunk */
		size_t globalSize[2] = { elemNum, 0 };
		size_t localSize[2] = { GLOBAL.localSize, 0 };

        for(unsigned int i = 2; i <= elemNum; i <<= 1) {
           for (unsigned int j = i>>1; j > 0; j = j>>1) {
               CL_ERR( clSetKernelArg(kernel, 1, sizeof(cl_uint), &j) );
               CL_ERR( clSetKernelArg(kernel, 2, sizeof(cl_uint), &i) );
		       ret |= clEnqueueNDRangeKernel(GLOBAL.clQueue, kernel, 1,
                              0, globalSize, localSize, 0, NULL, NULL);
           }
        }
		CL_ERR( ret );

		/* wait for all enqueued operations to finish */
		CL_ERR( clFinish(GLOBAL.clQueue) );
        
        //GLOBAL.profiler.start("GPU-MAP");
        ptrDataCheck = (float*) clEnqueueMapBuffer(GLOBAL.clQueue, gpu_mem, CL_TRUE,
                                                   CL_MAP_READ | CL_MAP_WRITE, 0, sizeof(float) * elemNum, 0, 0, 0, &ret);
        //GLOBAL.profiler.stop("GPU-MAP");
        CL_ERR( ret );
        DIE(ptrDataCheck != ptrData, "mapped ptr is different");

		return true;
	}

	bool bitonicSortPartial(float* ptrData, size_t elemNum, size_t elemChunk, bool useLDS)
	{
        /* use LDS memory or use directly the global vram/ram */
        cl_kernel kernelSortPartial = (useLDS) ? kernel_lds : kernel_nolds;
        
        DIE(elemChunk != GLOBAL.chunkSize, "mistmatch chunk size");
        cl_uint elemPerWorkItem = GLOBAL.chunkSize / GLOBAL.localSize;
        float* ptrDataCheck = NULL;

        //GLOBAL.profiler.start("GPU-HOST-PTR");
        cl_mem gpu_mem = clCreateBuffer(GLOBAL.clContext, 
            CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(float) * elemNum, ptrData, &ret);
        //GLOBAL.profiler.stop("GPU-HOST-PTR");
        CL_ERR( ret );
        
		CL_ERR( clSetKernelArg(kernelSortPartial, 0, sizeof(gpu_mem), &gpu_mem) );
        CL_ERR( clSetKernelArg(kernelSortPartial, 1, sizeof(elemPerWorkItem), &elemPerWorkItem) );
        
        /* Each group of work-items processes a chunk */
        size_t globalSize[2] = { elemNum / elemPerWorkItem, 0 };
        size_t localSize[2] = { GLOBAL.localSize, 0 };

		ret = clEnqueueNDRangeKernel(GLOBAL.clQueue, kernelSortPartial, 1, 0, globalSize, localSize, 0, NULL, NULL);
		CL_ERR( ret );

		/* wait for all enqueued operations to finish */
		CL_ERR( clFinish(GLOBAL.clQueue) );

        //GLOBAL.profiler.start("GPU-MAP");
        ptrDataCheck = (float*) clEnqueueMapBuffer(GLOBAL.clQueue, gpu_mem, CL_TRUE, 
            CL_MAP_READ | CL_MAP_WRITE, 0, sizeof(float) * elemNum, 0, 0, 0, &ret);
        //GLOBAL.profiler.stop("GPU-MAP");
        
        CL_ERR( ret );
        DIE(ptrDataCheck != ptrData, "mapped ptr is different");
		
		return true;
	}
};

/************************************
	CPU merge alg
************************************/
void cpuMerge(float* ptrData,
              float* ptrDataMerge,
              int left,
              int right)
{
    int i, j, k;
    int mid = left / 2 + right / 2;
    
    // copy data to temp merge area
    for (i = left; i < right; i++)
        ptrDataMerge[i] = ptrData[i];
    
    i = left;   // Initial index of first subarray
    j = mid;    // Initial index of second subarray
    k = left;   // Initial index of merged subarray
    
    while (i < mid && j < right) {
        if (ptrDataMerge[i] <= ptrDataMerge[j]) {
            ptrData[k] = ptrDataMerge[i];
            i++;
        }
        else {
            ptrData[k] = ptrDataMerge[j];
            j++;
        }
        k++;
    }
    
    while (i < mid) {
        ptrData[k] = ptrDataMerge[i];
        i++;
        k++;
    }
    
    while (j < right) {
        ptrData[k] = ptrDataMerge[j];
        j++;
        k++;
    }
}

void cpuMergeSort(float* ptrData,
                  float* ptrDataMerge,
                  int left,
                  int right)
{
    if (left + 1 < right) {
        int mid = left / 2 + right / 2;
        
        cpuMergeSort(ptrData, ptrDataMerge, left, mid);
        cpuMergeSort(ptrData, ptrDataMerge, mid, right);
        cpuMerge(ptrData, ptrDataMerge, left, right);
    }
}

/************************************
	GPU-CPU merge alg
 ************************************/
void cpuMergeSortPartial(float* ptrData,
                         float* ptrDataMerge,
                         int left,
                         int right,
                         int limit)
{
    if( (right - left) > limit)
    {
        int mid = left / 2 + right / 2;
        cpuMergeSortPartial(ptrData, ptrDataMerge, left, mid, limit);
        cpuMergeSortPartial(ptrData, ptrDataMerge, mid, right, limit);
    }
    
    /* start merge at 2 chunks of 2048 each */
    cpuMerge(ptrData, ptrDataMerge, left, right);
}

/************************************
 CPU merge sort
 ************************************/
void cpuAlgMerge(float* ptrData, int elemNum, int chunkNum)
{
    float* ptrDataMerge = new float[elemNum];
    DIE(ptrDataMerge == NULL, "alloc failed");
    
    if(elemNum == chunkNum) {
        cpuMergeSort(ptrData, ptrDataMerge, 0, elemNum);
    } else {
        cpuMergeSortPartial(ptrData, ptrDataMerge, 0, elemNum, chunkNum);
    }
    
    delete[] ptrDataMerge;
}

/**
* MAIN function (CPU/HOST)
*/
int main(int argc, char** argv)
{
    vector<float> vecValidate;
	vector<float> vecData;
    GPU *gpu;

    /* parse args */
    read_args(argc, argv);

    /*****************************  ALLOCATE MEMORY *****************************/
    
    DIE(vecData.max_size() < GLOBAL.globalSize, "cannot resize to that");
    vecData.resize(GLOBAL.globalSize);

    // uses separate VRAM space
    if(GLOBAL.isAlgGpuBitonicLDS || GLOBAL.isAlgGpuBitonicNoLDS || GLOBAL.isAlgGpuBitonicFull) {
        gpuGlobalInit(GLOBAL.platform, GLOBAL.device);
        
        /* init GPU, compile kernel */
        gpu = new GPU();
        DIE(gpu == NULL, "gpu is NULL");
    }

    /* fill array */
	srand (time(NULL));
	for(unsigned int i=0; i<GLOBAL.globalSize; i++) {
        vecData[i] = (float) (rand() % 90000) * 0.13f - 45000.f;
    }
    
    if(GLOBAL.verbose >= 3) {
        cout << "Validate with STL" << endl;
        vecValidate.resize(GLOBAL.globalSize);
        
        for(unsigned int i=0; i<GLOBAL.globalSize; i++) {
            vecValidate[i] = vecData[i];
        }
    }

	printCheck(vecData);
    
    /*****************************  START-SORT    *****************************/
    
    GLOBAL.profiler.start("TOTAL");
    
    /*****************************  PRE-SORT DATA    *****************************/
    
    if(GLOBAL.isAlgGpuBitonicLDS)
    {
	DIE(gpu == NULL, "GPU not initialized");
        
	GLOBAL.profiler.start("GPU-LDS-bitonic");
	gpu->bitonicSortPartial(&vecData[0], GLOBAL.globalSize, GLOBAL.chunkSize, true);
        GLOBAL.profiler.stop("GPU-LDS-bitonic");
    }
    else if (GLOBAL.isAlgGpuBitonicNoLDS)
    {
        DIE(gpu == NULL, "GPU not initialized");
        
	GLOBAL.profiler.start("GPU-NoLDS-bitonic");
        gpu->bitonicSortPartial(&vecData[0], GLOBAL.globalSize, GLOBAL.chunkSize, false);
        GLOBAL.profiler.stop("GPU-NoLDS-bitonic");
    }
    else if (GLOBAL.isAlgGpuBitonicFull)
    {
        DIE(gpu == NULL, "GPU not initialized");

        GLOBAL.profiler.start("GPU-NoLDS-full");
        gpu->bitonicSort(&vecData[0], GLOBAL.globalSize);
        GLOBAL.profiler.stop("GPU-NoLDS-full");
    } else {
        GLOBAL.profiler.start("GPU-N/A");
        GLOBAL.profiler.stop("GPU-N/A");
    }
    
    /*****************************  SORT DATA    *****************************/

    /* CPU partial merge */
    if(GLOBAL.isAlgPartialMerge)
    {
		GLOBAL.profiler.start("CPU-partial-mergesort");
      	cpuAlgMerge(&vecData[0], GLOBAL.globalSize, GLOBAL.chunkSize);
    	GLOBAL.profiler.stop("CPU-partial-mergesort");
	}
     /* CPU full merge */
    else if(GLOBAL.isAlgFullMerge)
    {
		GLOBAL.profiler.start("CPU-full-mergesort");
      	cpuAlgMerge(&vecData[0], GLOBAL.globalSize, GLOBAL.globalSize);
    	GLOBAL.profiler.stop("CPU-full-mergesort");
	}
    /* CPU STL sort */
    else if(GLOBAL.isAlgStlSort)
    {
        GLOBAL.profiler.start("CPU-stl-sort");
        std::sort (vecData.begin(), vecData.end());
        GLOBAL.profiler.stop("CPU-stl-sort");
    }

    /* CPU parallel STL sort */
    else if(GLOBAL.isAlgParStlSort)
    {
#if __GNU__
        GLOBAL.profiler.start("CPU-par-stl-sort");
        __gnu_parallel::sort(vecData.begin(), vecData.end());
        GLOBAL.profiler.stop("CPU-par-stl-sort");
#else
        DIE(1, "GNU parallel STL required");
#endif

    }

    /* CPU STL qsort */
    else if(GLOBAL.isAlgStlQSort)
    {
        GLOBAL.profiler.start("CPU-stl-qsort");
        std::sort (vecData.begin(), vecData.end());
        GLOBAL.profiler.stop("CPU-stl-qsort");
    }
    /* CPU Time sort */
    else if(GLOBAL.isAlgTimSort)
    {
        GLOBAL.profiler.start("CPU-tim-sort");
        gfx::timsort(vecData.begin(), vecData.end());
        GLOBAL.profiler.stop("CPU-tim-sort");
    }
    
    
    /*****************************  END-SORT    *****************************/
    
    GLOBAL.profiler.stop("TOTAL");
    
    DIE(printCheck(vecData) > 0, "sorting failed");
    GLOBAL.profiler.printStats();
    
    if(GLOBAL.verbose >= 3) {
        cout << "Start validation with STL" << endl;
        std::sort (vecValidate.begin(), vecValidate.end());
        
        for(size_t i = 0; i < GLOBAL.globalSize; i++) {
            DIE(vecValidate[i] != vecData[i], "Invalid sort");
        }
    }

	return 0;
}
