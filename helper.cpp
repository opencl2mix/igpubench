#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <string>
#include <unistd.h>
#include "helper.hpp"


using namespace std;

/**
 * Global default settings
 */
Global::Global() 
{
    platform = 0;
    device = 0;
    useSvm = false;
    globalSize = 4096;
    chunkSize = 2048;
    localSize = 256;
    verbose = 0;
    
    /* default pre-sort -> GPU bitonic LDS */
    isAlgGpuBitonicLDS = false;
    isAlgGpuBitonicNoLDS = false;
    isAlgGpuBitonicFull = false;

    /* default sort -> partial merge */
    isAlgPartialMerge = false;
    isAlgFullMerge = false;
    isAlgStlSort = false;
    isAlgStlQSort = false;
    isAlgTimSort = false;
}

struct Global GLOBAL;

/**
 * Profiling functions
 */

Profiler::Profiler() {
    this->startOfTime = chrono::steady_clock::now();
}

void Profiler::start(string name) {
    auto diffTime = chrono::duration_cast<chrono::milliseconds>( 
                        chrono::steady_clock::now() - startOfTime );
    ProfileEvent profileEvent;

    profileEvent.start = diffTime.count();

    if(tmpListExecTime.count(name) == 0)
        tmpListExecTime[ name ] = profileEvent;
}

void Profiler::stop(string name) {
    auto diffTime = chrono::duration_cast<chrono::milliseconds>( 
                chrono::steady_clock::now() - startOfTime );

    if( tmpListExecTime.count(name) == 0) {
        cout << "Invalid profile stop " << name << endl;
        return;
    } else {
        ProfileEvent profileEvent = tmpListExecTime[ name ];
        profileEvent.stop = diffTime.count();

        // erase from tmp, add to final
        tmpListExecTime.erase( tmpListExecTime.find( name ) );
        listExecTime[ name ].push_back( profileEvent );
    }
}

void Profiler::printStats() 
{
    for(auto it = listExecTime.begin(); it != listExecTime.end(); ++it) 
    {
        if(GLOBAL.verbose) { 
            cout << it->first << " (ms) details: " << endl;
        }
        long long sumTime = 0;
        auto listEvents = it->second;
        for(auto ev : listEvents) {

            if(GLOBAL.verbose) {
                cout << ev.start << " -> " << ev.stop <<", " << (ev.stop - ev.start) << endl;
            }
            sumTime += (ev.stop - ev.start);
        }

        if(GLOBAL.verbose) {
            cout  << it->first << ", total: " << sumTime
                << "ms, avg " << (sumTime / listEvents.size()) << "ms" << endl;
            cout << endl;
        }
        else {
             cout << ", " << it->first << ", " << sumTime;
        }
    }

    if(GLOBAL.verbose) {
        cout << "----------------" << endl;
    } else {
        cout << endl;
    }
}


/**
 * User/host function, check OpenCL function return code
 */
int CL_ERR(int cl_ret)
{
    if(cl_ret != CL_SUCCESS){
        cout << endl << cl_get_string_err(cl_ret) << endl;
        return 1;
    }
    return 0;
}

/**
 * User/host function, check OpenCL compilation return code
 */
int CL_COMPILE_ERR(int cl_ret,
                  cl_program program,
                  cl_device_id device)
{
    if(cl_ret != CL_SUCCESS){
        cout << endl << cl_get_string_err(cl_ret) << endl;
        cl_get_compiler_err_log(program, device);
        return 1;
    }
    return 0;
}

/**
* Read kernel from file
*/
void read_kernel(string file_name, string &str_kernel)
{
    ifstream in_file(file_name.c_str());
    in_file.open(file_name.c_str());
    DIE( !in_file.is_open(), "open file" );

    stringstream str_stream;
    str_stream << in_file.rdbuf();

    str_kernel = str_stream.str();
}

/**
 * OpenCL return error message, used by CL_ERR and CL_COMPILE_ERR
 */
const char* cl_get_string_err(cl_int err) {
switch (err) {
    case CL_SUCCESS:                        return  "Success!";
    case CL_DEVICE_NOT_FOUND:               return  "Device not found.";
    case CL_DEVICE_NOT_AVAILABLE:           return  "Device not available";
    case CL_COMPILER_NOT_AVAILABLE:         return  "Compiler not available";
    case CL_MEM_OBJECT_ALLOCATION_FAILURE:  return  "Memory object alloc fail";
    case CL_OUT_OF_RESOURCES:               return  "Out of resources";
    case CL_OUT_OF_HOST_MEMORY:             return  "Out of host memory";
    case CL_PROFILING_INFO_NOT_AVAILABLE:   return  "Profiling information N/A";
    case CL_MEM_COPY_OVERLAP:               return  "Memory copy overlap";
    case CL_IMAGE_FORMAT_MISMATCH:          return  "Image format mismatch";
    case CL_IMAGE_FORMAT_NOT_SUPPORTED:     return  "Image format no support";
    case CL_BUILD_PROGRAM_FAILURE:          return  "Program build failure";
    case CL_MAP_FAILURE:                    return  "Map failure";
    case CL_INVALID_VALUE:                  return  "Invalid value";
    case CL_INVALID_DEVICE_TYPE:            return  "Invalid device type";
    case CL_INVALID_PLATFORM:               return  "Invalid platform";
    case CL_INVALID_DEVICE:                 return  "Invalid device";
    case CL_INVALID_CONTEXT:                return  "Invalid context";
    case CL_INVALID_QUEUE_PROPERTIES:       return  "Invalid queue properties";
    case CL_INVALID_COMMAND_QUEUE:          return  "Invalid command queue";
    case CL_INVALID_HOST_PTR:               return  "Invalid host pointer";
    case CL_INVALID_MEM_OBJECT:             return  "Invalid memory object";
    case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:return  "Invalid image format desc";
    case CL_INVALID_IMAGE_SIZE:             return  "Invalid image size";
    case CL_INVALID_SAMPLER:                return  "Invalid sampler";
    case CL_INVALID_BINARY:                 return  "Invalid binary";
    case CL_INVALID_BUILD_OPTIONS:          return  "Invalid build options";
    case CL_INVALID_PROGRAM:                return  "Invalid program";
    case CL_INVALID_PROGRAM_EXECUTABLE:     return  "Invalid program exec";
    case CL_INVALID_KERNEL_NAME:            return  "Invalid kernel name";
    case CL_INVALID_KERNEL_DEFINITION:      return  "Invalid kernel definition";
    case CL_INVALID_KERNEL:                 return  "Invalid kernel";
    case CL_INVALID_ARG_INDEX:              return  "Invalid argument index";
    case CL_INVALID_ARG_VALUE:              return  "Invalid argument value";
    case CL_INVALID_ARG_SIZE:               return  "Invalid argument size";
    case CL_INVALID_KERNEL_ARGS:            return  "Invalid kernel arguments";
    case CL_INVALID_WORK_DIMENSION:         return  "Invalid work dimension";
    case CL_INVALID_WORK_GROUP_SIZE:        return  "Invalid work group size";
    case CL_INVALID_WORK_ITEM_SIZE:         return  "Invalid work item size";
    case CL_INVALID_GLOBAL_OFFSET:          return  "Invalid global offset";
    case CL_INVALID_EVENT_WAIT_LIST:        return  "Invalid event wait list";
    case CL_INVALID_EVENT:                  return  "Invalid event";
    case CL_INVALID_OPERATION:              return  "Invalid operation";
    case CL_INVALID_GL_OBJECT:              return  "Invalid OpenGL object";
    case CL_INVALID_BUFFER_SIZE:            return  "Invalid buffer size";
    case CL_INVALID_MIP_LEVEL:              return  "Invalid mip-map level";
    default:                                return  "Unknown";
    }
}

/**
 * Check compiler return code, used by CL_COMPILE_ERR
 */
void cl_get_compiler_err_log(cl_program program,
                             cl_device_id device)
{
    char* build_log;
    size_t log_size;

    /* first call to know the proper size */
    clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
    build_log = new char[ log_size + 1 ];

    /* second call to get the log */
    clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, log_size, build_log, NULL);
    build_log[ log_size ] = '\0';
    cout << endl << build_log << endl;
}

/**
* Retrieve GPU device
*/
void gpuGlobalInit(uint platform_select, uint device_select)
{
    int ret;
    cl_device_id device;
    cl_platform_id platform;
    cl_uint platform_num = 0;
    cl_platform_id* platform_list = NULL;

    cl_uint device_num = 0;
    cl_device_id* device_list = NULL;

    size_t attr_size = 0;
    cl_char* attr_data = NULL;

    /* get num of available OpenCL platforms */
    CL_ERR( clGetPlatformIDs(0, NULL, &platform_num) );
    platform_list = new cl_platform_id[platform_num];
    DIE(platform_list == NULL, "alloc platform_list");

    /* get all available OpenCL platforms */
    CL_ERR( clGetPlatformIDs(platform_num, platform_list, NULL) );
    if(GLOBAL.verbose) {
        cout << "Platforms found: " << platform_num << endl;
    }
    /* list all platforms and VENDOR/VERSION properties */
    for(uint platf=0; platf<platform_num; platf++)
    {
        /* get attribute CL_PLATFORM_VENDOR */
        CL_ERR( clGetPlatformInfo(platform_list[platf],
                CL_PLATFORM_VENDOR, 0, NULL, &attr_size));
        attr_data = new cl_char[attr_size];
        DIE(attr_data == NULL, "alloc attr_data");

        /* get data CL_PLATFORM_VENDOR */
        CL_ERR( clGetPlatformInfo(platform_list[platf],
                CL_PLATFORM_VENDOR, attr_size, attr_data, NULL) );
        if(GLOBAL.verbose) {
            cout << "Platform " << platf << " " << attr_data << " ";
        }
        delete[] attr_data;

        /* get attribute size CL_PLATFORM_VERSION */
        CL_ERR( clGetPlatformInfo(platform_list[platf],
                CL_PLATFORM_VERSION, 0, NULL, &attr_size));
        attr_data = new cl_char[attr_size];
        DIE(attr_data == NULL, "alloc attr_data");

        /* get data size CL_PLATFORM_VERSION */
        CL_ERR( clGetPlatformInfo(platform_list[platf],
                CL_PLATFORM_VERSION, attr_size, attr_data, NULL));
        if(GLOBAL.verbose) {
            cout << attr_data << endl;
        }
        delete[] attr_data;

        /* no valid platform found */
        platform = platform_list[platf];
        DIE(platform == 0, "platform selection");

        /* get num of available OpenCL devices type GPU on the selected platform */
        CL_ERR( clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 0, NULL, &device_num));
        device_list = new cl_device_id[device_num];
        DIE(device_list == NULL, "alloc devices");

        /* get all available OpenCL devices type GPU on the selected platform */
        CL_ERR( clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL,
              device_num, device_list, NULL));
        if(GLOBAL.verbose) {
            cout << "\tDevices found " << device_num  << endl;
        }
        /* list all devices and TYPE/VERSION properties */
        for(uint dev=0; dev<device_num; dev++)
        {
            /* get attribute size */
            CL_ERR( clGetDeviceInfo(device_list[dev], CL_DEVICE_NAME,
                0, NULL, &attr_size));
            attr_data = new cl_char[attr_size];
            DIE(attr_data == NULL, "alloc attr_data");

            /* get attribute CL_DEVICE_NAME */
            CL_ERR( clGetDeviceInfo(device_list[dev], CL_DEVICE_NAME,
                attr_size, attr_data, NULL));
            if(GLOBAL.verbose) {
                cout << "\tDevice " << dev << " " << attr_data << " ";
            }
            delete[] attr_data;

            /* get attribute size */
            CL_ERR( clGetDeviceInfo(device_list[dev], CL_DEVICE_VERSION,
                0, NULL, &attr_size));
            attr_data = new cl_char[attr_size];
            DIE(attr_data == NULL, "alloc attr_data");

            /* get attribute CL_DEVICE_VERSION */
            CL_ERR( clGetDeviceInfo(device_list[dev], CL_DEVICE_VERSION,
                attr_size, attr_data, NULL));
            if(GLOBAL.verbose) {
                cout << attr_data; 
            }
            delete[] attr_data;

            /* select device based on cli arguments */
            if((platf == platform_select) && (dev == device_select)){
                device = device_list[dev];
                if(GLOBAL.verbose) {
                    cout << " <--- selected OpenCL device";
                }
            }
            if(GLOBAL.verbose) {
                cout << endl;
            }
        }
    }

    delete[] platform_list;
    delete[] device_list;

    /* finish GPU global init */
    GLOBAL.clDevice = device;

    /* create a context for the device */
    GLOBAL.clContext = clCreateContext(0, 1, &device, NULL, NULL, &ret);
    CL_ERR( ret );

    /* create a command queue for the device in the context */
#ifndef HAS_DEPRECATED
    GLOBAL.clQueue = clCreateCommandQueueWithProperties(GLOBAL.clContext, device, 0, &ret);
#else
    GLOBAL.clQueue = clCreateCommandQueue(GLOBAL.clContext, device, 0, &ret);
#endif
    CL_ERR( ret );
}

/**
 * Parse program arguments
 */
void display_help() 
{
    cout << "Showcase application CPU/GPU" << endl;
    cout << "Grigore Lupescu, grigore.lupescu@gmail.com" << endl;
    cout << "\t-s\tvector size" << endl;
    
    cout << "\t-p\tplatform id" << endl;
    cout << "\t-d\tdevice id" << endl;
    
    cout << "\t-s\tvector size in exp2, 10=>2^10=>1024" << endl;
    cout << "\t-c\tchunk size in exp2, 9=>2^9=>512" << endl;
    cout << "\t-l\tlocal size in exp2, 7=>2^7=>128" << endl;
    
    cout << "\t-g\t GPU alg (pre-sort): "
    "\n\t\t\t0 => bitonic-lds"
    "\n\t\t\t1 => bitonic-no-lds"
    "\n\t\t\t2 => bitonic-full" << endl;
    
    cout << "\t-a\t CPU alg (sort): "
      "\n\t\t\t0 => partial-mergesort"
      "\n\t\t\t1 => full-mergesort"
      "\n\t\t\t2 => STL-sort"
      "\n\t\t\t3 => STL-qsort"
      "\n\t\t\t4 => TIM-sort"
      "\n\t\t\t5 => ParSTL-sort" << endl;
    
    cout << "\t-h\tprint help" << endl;
}


void read_args(int argc, char** argv) 
{
    int opt = 0;
    const char *optString = "vh:a:g:p:d:s:c:l:?";

    while( opt != -1 ) {
        switch(opt)
        {
            case 'v':
                GLOBAL.verbose++;
            break;
            
            case 'p':
                GLOBAL.platform = atoi(optarg);
            break;

            case 'd':
                GLOBAL.device = atoi(optarg);
            break;
            
            case 'l':
                DIE(atoi(optarg) > 10, "local size too big > 2^10");
                GLOBAL.localSize = 1 << atoi(optarg);
            break;
            
            case 'c':
                DIE(atoi(optarg) > 30, "chunk size too big > 2^30");
                GLOBAL.chunkSize = 1 << atoi(optarg);
            break;

            case 's':
                DIE(atoi(optarg) > 32, "vector size too big > 2^32");
                GLOBAL.globalSize = 1 << atoi(optarg);
            break;

            case 'g': {
                GLOBAL.isAlgGpuBitonicLDS = false;
                GLOBAL.isAlgGpuBitonicNoLDS = false;
                GLOBAL.isAlgGpuBitonicFull = false;
                
                if(atoi(optarg) == 0) {
                    GLOBAL.isAlgGpuBitonicLDS = true;
                } else if(atoi(optarg) == 1) {
                    GLOBAL.isAlgGpuBitonicNoLDS = true;
                } else if(atoi(optarg) == 2) {
                    GLOBAL.isAlgGpuBitonicFull = true;
                }
            }
            break;
            
            case 'a': {
                GLOBAL.isAlgPartialMerge = false;
                GLOBAL.isAlgFullMerge = false;
                GLOBAL.isAlgStlSort = false;
                GLOBAL.isAlgStlQSort = false;
                GLOBAL.isAlgTimSort = false;
                GLOBAL.isAlgParStlSort = false;
    
                if(atoi(optarg) == 0) {
                    GLOBAL.isAlgPartialMerge = true;
                } else if(atoi(optarg) == 1) {
                    GLOBAL.isAlgFullMerge = true;
                } else if(atoi(optarg) == 2) {
                    GLOBAL.isAlgStlSort = true;
                } else if(atoi(optarg) == 3) {
                    GLOBAL.isAlgStlQSort = true;
                } else if(atoi(optarg) == 4) {
                    GLOBAL.isAlgTimSort = true;
                } else if(atoi(optarg) == 5) {
                  GLOBAL.isAlgParStlSort = true;
                }
            }
            break;

            case 'h':
            case '?':
                display_help();
                exit(1);
            break;

            default:
            break;
        }
        opt = getopt( argc, argv, optString );
    }

    /* list selected platform and devices */
    if(GLOBAL.verbose) {
        cout << "ARGS (svm = " << GLOBAL.useSvm << ", platform = " << GLOBAL.platform 
             << ", device = " << GLOBAL.device << ", ";
    }
    
    if(GLOBAL.verbose) {
        cout << "CONFIG -> Global, Chunk, Local : ";
    }
    if(GLOBAL.globalSize > (1024 * 1024)) {
        cout << GLOBAL.globalSize / (1024 * 1024) << "M, ";
    } else if(GLOBAL.globalSize > (1024)) {
        cout << GLOBAL.globalSize / (1024) << "K, ";
    }else {
        cout << GLOBAL.globalSize << ", ";
    }
    
    cout << GLOBAL.chunkSize << ", " << GLOBAL.localSize;

    if(GLOBAL.verbose) {
        cout << ")" << endl;
    }
    
    int divExact = 0;
    
    divExact = GLOBAL.globalSize / GLOBAL.chunkSize;
    DIE(GLOBAL.globalSize != (divExact * GLOBAL.chunkSize), "Global size must be multiple of chunk size");
    
    divExact = GLOBAL.chunkSize / GLOBAL.localSize;
    DIE(GLOBAL.chunkSize != (divExact * GLOBAL.localSize), "Chunk size must be multiple of local size");
    

    if(GLOBAL.verbose) {
        
        cout << " PRE-SORT (GPU): ";
        if(GLOBAL.isAlgGpuBitonicLDS) {
            cout << "Pre-sort Bitonic LDS" << endl;
        } else if(GLOBAL.isAlgGpuBitonicNoLDS) {
            cout << "Pre-sort Bitonic no LDS" << endl;
        } else if(GLOBAL.isAlgGpuBitonicFull) {
            cout << "Pre-sort Bitonic FULL" << endl;
        } else {
            cout << "N/A" << endl;
        }
        
        cout << " SORT (CPU): ";
        if(GLOBAL.isAlgPartialMerge) {
            cout << "Algorithm Partial Merge" << endl;
        } else if(GLOBAL.isAlgFullMerge) {
            cout << "Algorithm Full Merge" << endl;
        } else if(GLOBAL.isAlgStlSort) {
            cout << "Algorithm STL Sort" << endl;
        } else if(GLOBAL.isAlgStlQSort) {
            cout << "Algorithm STL QSort" << endl;
        } else if(GLOBAL.isAlgTimSort) {
            cout << "Algorithm TIM Sort" << endl;
        } else {
            cout << "N/A" << endl;
        }
    }
}
