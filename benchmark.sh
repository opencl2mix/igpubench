#!/bin/bash
for vecSize in {21..27}; do

    for cpuAlg in {1..4}; do
        # Full-Merge
        ./igpuSort -a $cpuAlg -s $vecSize

        # Partial-Merge
        # LDS
        for gpuAlg in {0..1}; do
            ./igpuSort -a $cpuAlg -g $gpuAlg -s $vecSize -c 12 -l 8 -d 1
        done
    done
done
 
