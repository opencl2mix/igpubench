__kernel void kernel_gflops(__global float* output)
{
    float seed = output[ get_global_id(0) ];
    for(int i=0; i<10; i++)
	    output[ get_global_id(0) ] = seed * (float) get_global_id(0);
}
