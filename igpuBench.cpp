#include <stdio.h>
#include <cstring>
#include <stdio.h>
#include <time.h>
#include <vector>
#include <iostream>
#include <string>
#include <chrono>
#include <map>
#include <algorithm>

#include "helper.hpp"

using namespace std;

#define DEBUG    1
#define WORK_SIZE (128 * 1024 * 1024)
#define NUM_TIMES 8 

template<typename T> void printRam(T* ptr) {    
#if DEBUG
    for(int i = 0; i < 8; i++)
        cout << ptr[i] << " ";
    cout << endl;
#endif
}

class Task
{
public:
    RamRegion *ramRegion;
    string name;
    bool isGpu;

    virtual bool init() { cout << "init N/A" << endl; return true; }
    virtual bool exec() { cout << "exec N/A" << endl; return true; }
};

class CpuTask : public Task
{
    float* ptrData;

public:
    CpuTask(RamRegion *ramRegion) {
        this->isGpu = false;
        this->ramRegion = ramRegion;
    }

    bool init(){ return true; }

    bool exec() {
        cout << "CPU -> in: ";

        GLOBAL.profiler.start("CPU-prepareRAM");
        float* ptrData = (float*) ramRegion->getCpuPtr("cpugpu");
        GLOBAL.profiler.stop("CPU-prepareRAM");

        printRam( ptrData );
        
        GLOBAL.profiler.start("CPU-exec");
        for(int i=0; i<WORK_SIZE; i+=512)
            ptrData[i] = i + ptrData[i];
        GLOBAL.profiler.stop("CPU-exec");
        //vector<float> vec ( ptrData, ptrData + WORK_SIZE );
        //sort( vec.begin(), vec.end() );
        //memcpy( ptrData, &vec[0], WORK_SIZE * sizeof(float) );
    
        return true;
    }
};


class GpuTask : public Task
{
public:
    int ret;
    float* ptrData;
    string kernel_src;
    cl_program program;
    cl_kernel kernel;

    GpuTask(RamRegion *ramRegion)
    { 
        this->isGpu = true;
        this->ramRegion = ramRegion;
    }

    virtual bool init()    
    {
        /* retrieve kernel source */
        read_kernel("device.cl", kernel_src);
        const char* kernel_c_str = kernel_src.c_str();
    
        /* create kernel program from source */
        program = clCreateProgramWithSource(GLOBAL.clContext, 1,
             &kernel_c_str, NULL, &ret);
        CL_ERR( ret );

        /* compile the program for the given set of devices */
        ret = clBuildProgram(program, 1, &GLOBAL.clDevice, "-cl-std=CL2.0", NULL, NULL);
        CL_COMPILE_ERR( ret, program, GLOBAL.clDevice );

        /* create kernel associated to compiled source kernel */
        kernel = clCreateKernel(program, "kernel_gflops", &ret);
        CL_ERR( ret );

        return true;
    }

    virtual bool exec()    
    {    
        float* ptrData = (float*) ramRegion->getCpuPtr("cpugpu");

        // print data before kernel exec
        cout << "GPU -> in: ";
        printRam( ptrData );

        GLOBAL.profiler.start("GPU-prepareVRAM");
        ptrData = (float*) ramRegion->getGpuPtr("cpugpu");
        
        /* set OpenCL kernel argument */
        if(ramRegion->isUMA("cpugpu") == false)
            CL_ERR( clSetKernelArg(kernel, 0, sizeof(cl_mem), &ptrData) );
#ifdef HAS_SVM
        else 
            CL_ERR( clSetKernelArgSVMPointer(kernel, 0, (void*) ptrData) );
#endif
        GLOBAL.profiler.stop("GPU-prepareVRAM");    

        /* Execute OpenCL kernel num_times */
        size_t globalSize[2] = { WORK_SIZE, 0 };
        size_t localSize[2] = { 64, 0 };    

        GLOBAL.profiler.start("GPU-exec");
        ret = clEnqueueNDRangeKernel(GLOBAL.clQueue, kernel, 1, NULL,
                globalSize, localSize, 0, NULL, NULL);
        CL_ERR( ret );

        /* wait for all enqueued operations to finish */
        CL_ERR( clFinish(GLOBAL.clQueue) );
        GLOBAL.profiler.stop("GPU-exec");
    
        return true;
    }
};

class Scheduler
{
    vector< Task* > listTasks;

public:
    bool addTask( Task* task ) 
    {
        listTasks.push_back( task );
        task->init();

        return true;
    }

    bool runAll() 
    {
        // point 0 in time
        for( auto task : listTasks )
            task->exec();

        // show global stats on profile
        GLOBAL.profiler.printStats();
            
        return true;
    }
};

/**
* MAIN function (CPU/HOST)
*/
int main(int argc, char** argv)
{
	Scheduler scheduler;

    /* parse args */
    read_args(argc, argv);

    /* search and select platform/devices in OpenCL */
    gpuGlobalInit(GLOBAL.platform, GLOBAL.device);

    //float* ptrData = ptrRam<float>( "cpu-gpu" );
    RamRegion ramRegion;
    ramRegion.alloc("cpugpu", WORK_SIZE * sizeof(float), GLOBAL.useSvm);

    float* ptrData = (float*) ramRegion.getCpuPtr("cpugpu");

    for(int i=0; i<WORK_SIZE; i++)
        ptrData[i] = 1.1f * i;

    GpuTask gpuTask( &ramRegion );
    CpuTask cpuTask( &ramRegion );

    // add tasks 
    for(int i = 0; i < NUM_TIMES; i++) {
        scheduler.addTask( &cpuTask );
        scheduler.addTask( &gpuTask );
    }
    
    // exec all task code
    scheduler.runAll();

    return 0;
}
