CC=g++
CFLAGS=-O3 -Wall -std=c++11 -DHAS_DEPRECATED #-DHAS_SVM 
#CFLAGS=-g -Wall -std=c++11 -DHAS_SVM
INC=-I/opt/AMDAPPSDK-3.0/include/ 
LIB=-L/opt/AMDAPPSDK-3.0/lib/x86_64/ -L/usr/local/cuda/lib64/

all: igpuBench igpuSort

igpuSort:    igpuSort.cpp helper.cpp
	$(CC) $(CFLAGS) -fopenmp $(INC) $(LIB) igpuSort.cpp helper.cpp -lOpenCL -o igpuSort

igpuBench: igpuBench.cpp igpuWrapperSVM.cpp helper.cpp
	$(CC) $(CFLAGS) $(INC) $(LIB) igpuBench.cpp igpuWrapperSVM.cpp helper.cpp -lOpenCL -o igpuBench

clean:
	rm -f igpuBench igpuSort
